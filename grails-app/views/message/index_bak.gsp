<!DOCTYPE html>
<html>
<head>

    <title>在线聊天</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" href="${request.contextPath}/assets/layer/mobile/need/layer.css" media="all">
    <link rel="stylesheet" href="${request.contextPath}/assets/layer/theme/default/layer.css" media="all">

    <script type="text/javascript" src="${request.contextPath}/assets/jquery-2.2.0.min.js?compile=false" ></script>
    <script type="text/javascript" src="${request.contextPath}/assets/layer/mobile/layer.js?compile=false" ></script>
    <script type="text/javascript" src="${request.contextPath}/assets/sockjs.js?compile=false" ></script>
    <script type="text/javascript" src="${request.contextPath}/assets/stomp.js?compile=false" ></script>
    <script type="text/javascript" src="${request.contextPath}/assets/spring-websocket.js?compile=false" ></script>
    <script type="text/javascript" src="${request.contextPath}/assets/layer/layer.js?compile=false" ></script>


    <style>
        *{
            font-family: Consolas;
        }
        ul{
            list-style: none;
        }
        ul li{text-align: right;margin-right:24px;}
        #send_message{width:60%;height:28px;margin:2px 20px;}
        .left {text-align: left;line-height: 40px;display:block;}
        .right {text-align: right;line-height:40px;display:block;}
        .left img {width: 50px;height:50px;float: left;display: inline-block}
        .right img {width: 50px;height:50px;float: right}
        .left section {
            margin: 15px 0px;
            position: relative;
            padding: 2px 15px;
            background: #ef960e;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
        }
        .left section:before {
            position: absolute;
            content: "";
            width: 0;
            height: 0;
            right: 100%;
            top: 38px;
            border-top: 13px solid transparent;
            border-right: 26px solid #ef960e;
            border-bottom: 13px solid transparent;
        }

        @media only screen and (max-width: 735px) {
            .left section {
                left: 48px;
                margin: 15px 0px;
                position: relative;
                padding: 2px 15px;
                background: #ef960e;
                -moz-border-radius: 12px;
                -webkit-border-radius: 12px;
                border-radius: 12px;
            }
            .left section:before {
                position: absolute;
                content: "";
                width: 0;
                height: 0;
                right: 100%;
                top: 38px;
                border-top: 13px solid transparent;
                border-right: 26px solid #ef960e;
                border-bottom: 13px solid transparent;
            }
            .right section {
                right: 48px;
                margin: 15px 0px;
                position: relative;
                padding: 2px 15px;
                background: #1E9FFF;
                -moz-border-radius: 12px;
                -webkit-border-radius: 12px;
                border-radius: 12px;
            }

            .right section:before {
                position: absolute;
                content: "";
                width: 0;
                height: 0;
                left: 100%;
                top: 38px;
                border-top: 13px solid transparent;
                border-left: 26px solid #1E9FFF;
                border-bottom: 13px solid transparent;
            }
        }

        .right section {
            margin: 15px 0px;
            position: relative;
            padding: 2px 15px;
            background: #1E9FFF;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
        }
        .right section:before {
            position: absolute;
            content: "";
            width: 0;
            height: 0;
            left: 100%;
            top: 38px;
            border-top: 13px solid transparent;
            border-left: 26px solid #1E9FFF;
            border-bottom: 13px solid transparent;
        }

        .right section p{display: block;}
        .right section span{display: block;font-size:10px;}

    </style>
    <script type="text/javascript">
        $(function() {

            let offset = []

            if(true) {
                if(($(window).height()<1366)&&($(window).width()<1100)) {
                    offset[0] = '98%';
                    offset[1] = '90%';
                }else{
                    offset[0] = '50%';
                    offset[1] = '80%';
                }

                load();

                $(window).resize(function(){
                    layer.closeAll();
                    if(($(window).height()<1366)&&($(window).width()<1100)) {
                        offset[0] = '98%';
                        offset[1] = '90%';
                    }else{
                        offset[0] = '50%';
                        offset[1] = '80%';
                    }
                    load();
                });
            }

            let sid = '${sid}';

            function load() {
                layer.open({
                    type: 1,
                    title: '匿名畅聊',
                    closeBtn: 0, //不显示关闭按钮
                    area: offset, //宽高
                    btn: ['发送','关闭'],
                    content: "<div><ul id='show_msg'></ul></div>",
                    btn3: function (index) {
                        close(index);
                    },
                    btn2: function(index){
                        let msg = $("#send_message").val().trim();
                        if(msg=='') {
                            parent.layer.msg("输入消息后才能发送！",{icon:5});
                            return false;
                        }

                        client.send("/app/hello", {}, JSON.stringify({'msg':$("#send_message").val().trim(),'sid':sid}));
                        $("#send_message").val('');
                        return false;
                    }

                });
                if($(window).width()<330) {
                    $(".layui-layer-btn0").before("<span class='layui-form'><div style='display: inline;float:left;width:100px;'><textarea type='text' style='width:100px;' id='send_message'/></textarea></div><span>");
                    // $(".layui-layer-btn0").css({"float":"left","display":"inline"});
                    // $(".layui-layer-btn1").css({"float":"left","display":"inline"});
                }else if(offset[0]=="98%") {
                    $(".layui-layer-btn0").before("<span class='layui-form'><div class='layui-input-inline' style='display: inline;'><textarea type='text' style='width:120px;' class='form-control' id='send_message' /></textarea></div><span>");
                }else{
                    $(".layui-layer-btn0").before("<span class='layui-form'><div class='layui-input-inline' style='display: inline'><textarea type='text' class='form-control' id='send_message'></textarea></div><span>");
                }
            }

            var socket = new SockJS("${createLink(uri: '/stomp')}");
            var client = Stomp.over(socket);

            client.connect({}, function() {
                client.subscribe("/topic/hello", function(message) {
                    let data = JSON.parse(message.body);
                    if(sid==data.sid) {
                        $("#show_msg").append("<li class='right'><div style='width:60%;display:inline-block;word-wrap: break-word;word-break: normal;'><section><p>" + data.msg + "</p></section></div><div style='width:10%;display:inline-block;margin:-28px auto;'><img src='${request.contextPath}/assets/global_anon.png'/></div></li>");
                    }else{
                        $("#show_msg").append("<li class='left'><div style='width:10%;display:inline-block;margin:-28px auto;'><img src='${request.contextPath}/assets/pink_anon.png'/></div><div style='width:60%;display:inline-block;word-wrap: break-word;word-break: normal;'><section><p>"+data.msg+"</p></section></div></li>");
                    }
                });
            });

        });
    </script>
</head>
<body>
    <p2>domo码云地址：<a href="https://gitee.com/lj18883588608/chat" target="_blank">立即前往</a></p2>
</body>
</html>