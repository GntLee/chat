<!DOCTYPE html>
<html>
<head>

    %{--<asset:javascript src="application" />--}%
    <asset:javascript src="jquery-2.2.0.min.js" />
    <asset:javascript src="spring-websocket" />
    <asset:javascript src="layer/layer.js"/>
    <title>系统公告</title>
    <style>
    ul{
        list-style: none;
    }
    ul li{text-align: right;margin-right:24px;}
    #send_message{width:60%;height:28px;}
    </style>
    <script type="text/javascript">
        $(function() {

            layer.open({
                type: 1,
                title: '系统公告',
                // closeBtn: 0, //不显示关闭按钮
                area: ['40%', '60%'], //宽高
                maxmin: true,
                content: "<div><ul id='show_msg'></ul></div>",
                btn3: function (index) {
                    layer.close(index);
                },
                btn2: function(index){
                    return false;
                }

            });
            $(".layui-layer-btn0").before("<input type='text' class='form-control' id='send_message' />");
            var socket = new SockJS("${createLink(uri: '/stomp')}");
            var client = Stomp.over(socket);

            client.connect({}, function() {
                client.subscribe("/topic/stockQuote", function(message) {
                    var obj = JSON.parse(message.body).content;
                    $("#show_msg").append("<li style='width: 100%;height: 40px;text-align:left;color: #1E9FFF;margin: 5px auto;padding: 25px 15px;background: #ccc'>"+obj.title+"<br/>"+obj.time+"</li>");
                });
            });

        });
    </script>
</head>
<body>
</body>
</html>