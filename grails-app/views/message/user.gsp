<!DOCTYPE html>
<html>
<head>
    %{--<meta name="layout" content="main"/>--}%

    <asset:javascript src="application" />
    <asset:javascript src="spring-websocket" />
    <asset:javascript src="layer/layer.js"/>
    <title>我的消息</title>
    <style>
    ul{
        list-style: none;
    }
    ul li{text-align: right;margin-right:24px;}
    #send_message{width:60%;height:28px;}
    </style>
    <script type="text/javascript">
        $(function() {

            layer.open({
                type: 1,
                title: '在线聊天',
                closeBtn: 0, //不显示关闭按钮
                area: ['40%', '60%'], //宽高
                btn: ['发送','关闭'],
                content: "<div><ul id='show_msg'></ul></div>",
                btn3: function (index) {
                    console.log("this is a test");
                    layer.close(index);
                },
                btn2: function(index){
                    console.log("..........sssssssssssss"+$("#send_message").val());
                    var msg = $("#send_message").val().trim();
                    if(msg=='') {
                        parent.layer.msg("输入消息后才能发送！",{icon:5});
                        return false;
                    }
//                    client.send("/app/hello", {}, $("#send_message").val().trim());
                    $("#send_message").val('');
                    for(var a=0;a<=1000;a++) {
                        client.send("/app/hello", {}, "测试10000条记录："+a);
                        setTimeout(function () {

                        },200)
                    }
                    return false;
                }

            });
            $(".layui-layer-btn0").before("<input type='text' class='form-control' id='send_message' />");
            var socket = new SockJS("${createLink(uri: '/stomp')}");
            var client = Stomp.over(socket);

            client.connect({}, function() {
                client.subscribe("/topic/hello", function(message) {
                    console.log("======111111");
                    $("#show_msg").append("<li>"+message.body+"</li>");
                });
            });

        });
    </script>
</head>
<body>
</body>
</html>