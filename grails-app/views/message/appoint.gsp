<!DOCTYPE html>
<html>
<head>

    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="spring-websocket"/>
    <asset:javascript src="layer/layer.js"/>
    <title>指定用户发送消息</title>
    <style>
    * {
        font-family: Consolas;
    }

    ul {
        list-style: none;
    }

    ul li {
        text-align: right;
        margin-right: 24px;
    }

    #send_message {
        width: 60%;
        height: 28px;
        margin: 2px 20px;
    }

    .left {
        text-align: left;
        line-height: 40px;
        display: block;
    }

    .right {
        text-align: right;
        line-height: 40px;
        display: block;
    }

    .left img {
        width: 50px;
        height: 50px;
        float: left;
        display: inline-block
    }

    .right img {
        width: 50px;
        height: 50px;
        float: right
    }

    .left section {
        margin: 15px 0px;
        position: relative;
        padding: 2px 15px;
        background: #ef960e;
        -moz-border-radius: 12px;
        -webkit-border-radius: 12px;
        border-radius: 12px;
    }

    .left section:before {
        position: absolute;
        content: "";
        width: 0;
        height: 0;
        right: 100%;
        top: 38px;
        border-top: 13px solid transparent;
        border-right: 26px solid #ef960e;
        border-bottom: 13px solid transparent;
    }

    @media only screen and (max-width: 735px) {
        .left section {
            left: 48px;
            margin: 15px 0px;
            position: relative;
            padding: 2px 15px;
            background: #ef960e;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
        }

        .left section:before {
            position: absolute;
            content: "";
            width: 0;
            height: 0;
            right: 100%;
            top: 38px;
            border-top: 13px solid transparent;
            border-right: 26px solid #ef960e;
            border-bottom: 13px solid transparent;
        }

        .right section {
            right: 48px;
            margin: 15px 0px;
            position: relative;
            padding: 2px 15px;
            background: #1E9FFF;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
        }

        .right section:before {
            position: absolute;
            content: "";
            width: 0;
            height: 0;
            left: 100%;
            top: 38px;
            border-top: 13px solid transparent;
            border-left: 26px solid #1E9FFF;
            border-bottom: 13px solid transparent;
        }
    }

    .right section {
        margin: 15px 0px;
        position: relative;
        padding: 2px 15px;
        background: #1E9FFF;
        -moz-border-radius: 12px;
        -webkit-border-radius: 12px;
        border-radius: 12px;
    }

    .right section:before {
        position: absolute;
        content: "";
        width: 0;
        height: 0;
        left: 100%;
        top: 38px;
        border-top: 13px solid transparent;
        border-left: 26px solid #1E9FFF;
        border-bottom: 13px solid transparent;
    }

    .right section p {
        display: block;
    }

    .right section span {
        display: block;
        font-size: 10px;
    }

    </style>
    <script type="text/javascript">
        $(function () {

            let sid = '${sid}';

            var socket = new SockJS("${createLink(uri: '/stomp')}");
            var client = Stomp.over(socket);

            layer.open({
                type: 1,
                title: '指定用户发送消息',
                // closeBtn: 0, //不显示关闭按钮
                area: ['40%', '60%'], //宽高
                maxmin: true,
                btn: ['发送', '关闭'],
                content: "<div><ul id='show_msg'></ul></div>",
                btn3: function (index) {
                    layer.close(index);
                },
                btn2: function (index) {
                    let msg = $("#send_message").val().trim();
                    if (msg == '') {
                        parent.layer.msg("输入消息后才能发送！", {icon: 5});
                        return false;
                    }

                    client.send("/queue/msg", {}, JSON.stringify({
                        'msg': $("#send_message").val().trim(),
                        'sid': sid,
                        "username": "user2"
                    }));
                    $("#send_message").val('');
                    return false;
                }

            });
            $(".layui-layer-btn0").before("<input type='text' class='form-control' id='send_message' />");

            client.connect({"username": "user2"}, function () {
                client.subscribe("/queue/msg", function (message) {
                    var obj = JSON.parse(message.body);
                    console.log(message);
                    console.log(obj);
                    console.log("用户id:" + sid);
                    console.log("用户id:" + obj.sid);
                    // if (sid == obj.sid) {
                    console.log(obj.username);
                    if ("user2" == obj.username) {
                        $("#show_msg").append("<li style='width: 100%;height: 40px;text-align:left;color: #1E9FFF;margin: 5px auto;padding: 25px 15px;background: #ccc'>" + obj.msg + "<br/></li>");
                    } else if(sid == obj.sid) {
                        $("#show_msg").append("<li style='width: 100%;height: 40px;text-align:right;color: #1E9FFF;margin: 5px auto;padding: 25px 15px;background: #ccc'>" + obj.msg + "<br/></li>");
                    }
                });
            });


        });
    </script>
</head>

<body>
</body>
</html>