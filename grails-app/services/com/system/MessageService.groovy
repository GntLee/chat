package com.system

import org.springframework.messaging.simp.SimpMessagingTemplate

class MessageService {


    SimpMessagingTemplate brokerMessagingTemplate

    void hello() {
        println 111111
        brokerMessagingTemplate.convertAndSend "/topic/hello", "hello from service!"
    }

    void sendToUser() {
        println 222222
        brokerMessagingTemplate.convertAndSendToUser("lee", "/queue/hello", "hello, target user!")
    }


}
