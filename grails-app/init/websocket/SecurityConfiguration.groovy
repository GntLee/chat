package websocket

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

import javax.annotation.PostConstruct

/**
 *
 * @Author Lee* @Description
 * @Date 2019年04月08日 23:31
 *
 */
@Configuration
@EnableWebSecurity
class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @PostConstruct
    void init() {
        println "初始化用户数据"
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Enable BASIC auth and prompt for credentials when accessing
        http.authorizeRequests()
                .antMatchers("/**")
                .authenticated()
                .and().httpBasic()
        // 禁用CSRF
        http.csrf().disable()
    }

    @Override
    @Autowired
    void configure(AuthenticationManagerBuilder auth) throws Exception {
        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> configurer = auth.inMemoryAuthentication();
        //Setup two dummy users - messages are automatically sent to "user" but not "user2"
        configurer.withUser("user").password("123456").roles("USER")
        configurer.withUser("user2").password("123456").roles("USER")
        configurer.withUser("user3").password("123456").roles("USER")
    }
}
