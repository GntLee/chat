package com.system

import groovy.json.JsonBuilder
import org.apache.commons.lang.RandomStringUtils
import org.apache.commons.lang.math.RandomUtils
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.context.request.RequestContextHolder

/**
 *  通过job推送公告
 */
class NoticeJob {

    SimpMessagingTemplate brokerMessagingTemplate

    static triggers = {
      simple repeatInterval: 5000 // execute job once in 5 seconds
    }


    def execute() {

        println "push begin."

        // 构建一个json对象
        def builder = new JsonBuilder()
        builder {
            title('关于春节放假通知')
            time(new Date().format("yyyy年MM月dd日 HH时mm分ss秒"))
        }

        def group =  "NoticeGroup"
        def description =  "模拟每5秒钟推送一次公告"

        // 通过websocket推送出去
        brokerMessagingTemplate.convertAndSend "/topic/stockQuote", builder

    }

}
